//Caelan Whitter

import java.util.Random;

public class RpsGame {
	
private int wins;
private int ties;
private int losses;
Random random = new Random();




public RpsGame(int wins, int ties, int losses) {
	this.wins=0;
	this.ties=0;
	this.losses=0;
	
}
public int getWins()
{
	return this.wins;
}
public int getTies()
{
	return this.ties;
}
public int getLosses()
{
	return this.losses;
}


public String playRound(String choice)
{
	String compChoice;
	int randNum = random.nextInt(3);
	if (randNum == 0)
	{
		compChoice = "rock";
	}
	else if (randNum == 1)
	{
		compChoice = "paper";
	}
	else
	{
		compChoice = "scissors";
	}
	
	if (choice.equals("rock") && compChoice.equals("paper"))
	{
		this.losses++;
		return "Computer plays "+compChoice+ " and the computer won";
	}
	else if (choice.equals("rock") && compChoice.equals("scissors"))
	{
		this.wins++;
		return "Computer plays "+compChoice+ " and the user won";
	}
	else if (choice.equals("paper") && compChoice.equals("rock"))
	{
		this.wins++;
		return "Computer plays "+compChoice+ " and the user won";
	}
	else if (choice.equals("paper") && compChoice.equals("scissors"))
	{
		this.losses++;
		return "Computer plays "+compChoice+ " and the computer won";
	}
	else if (choice.equals("scissors") && compChoice.equals("paper"))
	{
		this.wins++;
		return "Computer plays "+compChoice+ " and the user won";
	}
	else if (choice.equals("scissors") && compChoice.equals("rock"))
	{
		this.losses++;
		return "Computer plays "+compChoice+ " and the computer won";
	}
	else
	{
		this.ties++;
		return "Computer plays "+compChoice+ " and there was a tie";
	}
	
}


}
