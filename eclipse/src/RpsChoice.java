//Caelan Whitter 
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;


public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField messages;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame game;
	
	public RpsChoice(TextField messages,TextField wins,TextField losses, TextField ties,RpsGame game,String choice)
	{
		this.messages=messages;
		this.wins=wins;
		this.losses=losses;
		this.ties=ties;
		this.game=game;
		this.choice=choice;
		
		
		
		
		
				
	}

	public void handle(ActionEvent e)
	{
		
		String mes = this.game.playRound(this.choice);
		this.messages.setText(mes);
		this.wins.setText("wins: "+this.game.getWins());
		this.losses.setText("losses: "+this.game.getLosses());
		this.ties.setText("losses: "+this.game.getTies());
	}
}
