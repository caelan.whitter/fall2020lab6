//Caelan Whitter

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	
	public void start(Stage stage) {
		Group root = new Group(); 
		RpsGame game = new RpsGame(0,0,0);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
		
		//buttons
		HBox buttons = new HBox();
		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissors = new Button("Scissors");
		buttons.getChildren().addAll(rock,paper,scissors);
		
		
		
		
		//textfields
		
		HBox textfields = new HBox();
		TextField welcome = new TextField();
		TextField wins = new TextField();
		TextField losses = new TextField();
		TextField ties = new TextField();
		welcome.setPrefWidth(260);
		wins.setPrefWidth(130);
		losses.setPrefWidth(130);
		ties.setPrefWidth(130);
		textfields.getChildren().addAll(welcome,wins,losses,ties);
		
		//vbox
		VBox everything = new VBox();
		everything.getChildren().addAll(buttons,textfields);
		
		//root
		root.getChildren().add(everything);
		
		RpsChoice action1 = new RpsChoice(welcome,wins,losses,ties,game,"rock");
		RpsChoice action2 = new RpsChoice(welcome,wins,losses,ties,game,"paper");
		RpsChoice action3 = new RpsChoice(welcome,wins,losses,ties,game,"scissors");

		
		rock.setOnAction(action1);
		paper.setOnAction(action2);
		scissors.setOnAction(action3);
		
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
